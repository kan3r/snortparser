import os
import mysql.connector
from mysql.connector import errorcode

from database import cur

DB_NAME = 'db'

if os.getenv('MY_SQL_DATABASE'):
    DB_NAME = 'testdb'

TABLES = {}

TABLES['rules'] = (
    "CREATE TABLE `rules` ("
    " `id` int(11) NOT NULL AUTO_INCREMENT,"
    " `msg` varchar(250) NOT NULL,"
    " `sid` int(11) NOT NULL UNIQUE,"
    " `cve` varchar(250),"
    " `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
    " PRIMARY KEY (`id`)"
    ") ENGINE=InnoDB"
)


def create_database():
    cur.execute(f"CREATE DATABASE IF NOT EXISTS {DB_NAME} DEFAULT CHARACTER SET 'utf8'")
    print(f"Database {DB_NAME} created")


def create_tables():
    cur.execute(f"USE {DB_NAME}")
    for table_name in TABLES:
        table_description = TABLES[table_name]
        try:
            print(f"Creating table rules")
            cur.execute(table_description)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Already exists")
            else:
                print(err)


create_database()
create_tables()