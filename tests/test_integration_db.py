import unittest

from snortparser.__main__ import add_rule_db, get_rules_db, delete_rule_db, delete_rules_db, read_rules
import setup


class TestDBFunctions(unittest.TestCase):
    def test_add_rule_db(self):
        path = "resources/data"
        rules_all = read_rules(path)
        rule = rules_all[2]
        add_rule_db(rule.msg, rule.sid, rule.cve)
        self.assertEqual(len(get_rules_db()), 3)

    def test_get_rules_db(self):
        path = "resources/data"
        rules_all = read_rules(path)
        [add_rule_db(rule.msg, rule.sid, rule.cve) for rule in rules_all]
        result = get_rules_db()
        self.assertEqual(len(result), 3)

    def test_delete_rule_db(self):
        delete_rule_db(118)
        remaining_rules = get_rules_db()
        self.assertEqual(len(remaining_rules), 2)

    def test_delete_rules_db(self):
        delete_rules_db()
        remaining_rules = get_rules_db()
        self.assertEqual(len(remaining_rules), 0)


if __name__ == '__main__':
    unittest.main(buffer=True)
