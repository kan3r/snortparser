import unittest
from snortparser.snortparser import SnortParser


class TestSnortParserMethods(unittest.TestCase):
    def setUp(self):
        valid_rule = 'alert tcp $EXTERNAL_NET any -> $HOME_NET 12345:12346 (msg:"MALWARE-BACKDOOR netbus getinfo"; flow:to_server,established; content:"GetInfo|0D|"; metadata:ruleset community; cve,2019-0001; classtype:trojan-activity; sid:110; rev:10;)'
        self.valid_rule = SnortParser(valid_rule)

    def test_rule_init_values(self):
        self.assertEqual(self.valid_rule.msg, "MALWARE-BACKDOOR netbus getinfo")
        self.assertEqual(self.valid_rule.cve, "2019-0001")
        self.assertEqual(self.valid_rule.sid, "110")

    def test_enable_rule(self):
        self.valid_rule.enable()
        self.assertTrue(self.valid_rule.rule.startswith('alert'))

    def test_disable_rule(self):
        self.valid_rule.disable()
        self.assertTrue(self.valid_rule.rule.startswith('#alert'))


class TestSnortParserCreation(unittest.TestCase):
    def test_exit_creation_on_invalid_rule(self):
        invalid_rule = 'alert tcp $EXTERNAL_NET any -> $HOME_NET 12345:12346 (msg:"MALWARE-BACKDOOR netbus getinfo"; flow:to_server,established; content:"GetInfo|0D|"; metadata:ruleset community;)'
        self.invalid_rule = SnortParser(invalid_rule)
        self.assertTrue(self.invalid_rule.rule.startswith('#rule not valid'))

    def test_successful_creation(self):
        valid_rule = 'alert tcp $EXTERNAL_NET any -> $HOME_NET 12345:12346 (msg:"MALWARE-BACKDOOR netbus getinfo"; flow:to_server,established; content:"GetInfo|0D|"; metadata:ruleset community; cve:2019-0001; classtype:trojan-activity; sid:110; rev:10;)'
        self.valid_rule = SnortParser(valid_rule)
        self.assertIsInstance(self.valid_rule, SnortParser)

    def test_class_method_is_valid_rule(self):
        valid_rule = 'alert tcp $EXTERNAL_NET any -> $HOME_NET 12345:12346 (msg:"MALWARE-BACKDOOR netbus getinfo"; flow:to_server,established; content:"GetInfo|0D|"; metadata:ruleset community; cve:2019-0001; classtype:trojan-activity; sid:110; rev:10;)'
        invalid_rule = 'alert tcp $EXTERNAL_NET any -> $HOME_NET 12345:12346 (msg:"MALWARE-BACKDOOR netbus getinfo"; flow:to_server,established; content:"GetInfo|0D|"; metadata:ruleset community;)'

        self.assertTrue(SnortParser.is_valid_rule(valid_rule))
        self.assertFalse(SnortParser.is_valid_rule(invalid_rule))


if __name__ == '__main__':
    unittest.main()
