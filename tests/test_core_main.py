import unittest
from snortparser.__main__ import read_rules


class TestMainFunctions(unittest.TestCase):
    def test_read_file_success(self):
        data_path = 'resources/data'
        processed_file = read_rules(data_path)
        self.assertEqual(type(processed_file), list)

    def test_read_file_failure(self):
        data_wrong_path = 'wrong_path/data'
        self.assertEqual(read_rules(data_wrong_path), "file not found")


if __name__ == '__main__':
    unittest.main()
