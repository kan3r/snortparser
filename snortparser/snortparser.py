import re


class SnortParser:
    def __init__(self, rule: str):
        self.rule = rule
        self.msg = ""
        self.cve = ""
        self.sid = ""
        self.parse_rule()

    def parse_rule(self):
        if SnortParser.is_valid_rule(self.rule):
            self.find_msg()
            self.find_sid()
            self.find_cve()
        else:
            self.rule = "#rule not valid " + self.rule

    def find_cve(self):
        cve_regex = r'(?<=cve,)[\d-]*(?=;)'
        if 'cve,' in self.rule:
            cve = re.search(cve_regex, self.rule)
            self.cve = cve.group(0)

    def find_sid(self):
        sid_regex = r'(?<=sid:)\d*(?=;)'
        sid = re.search(sid_regex, self.rule)
        self.sid = sid.group(0)

    def find_msg(self):
        msg_regex = r'(?<=msg:")[^";]*(?=";)'
        msg = re.search(msg_regex, self.rule)
        self.msg = msg.group(0)

    def enable(self):
        if self.rule.startswith('#'):
            self.rule = self.rule[1:]

    def disable(self):
        if not self.rule.startswith('#'):
            self.rule = '#' + self.rule

    def __str__(self):
        return f"msg={self.msg} sid={self.sid} cve={self.cve if self.cve else 'None'}"

    def __repr__(self):
        return f"<Snort msg={self.msg} sid={self.sid} cve={self.cve if self.cve else 'None'} >"

    @classmethod
    def is_valid_rule(cls, rule: str) -> bool:
        return "classtype:" in rule and "sid:" in rule
