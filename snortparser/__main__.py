from snortparser.snortparser import SnortParser
from database import con, cur

TEST_DATA = "resources/data"


def read_rules(path):
    try:
        rules = []
        with open(path, "r") as f:
            data = f.readlines()
            for line in data:
                if (line.startswith("alert") or line.startswith("# alert")) and SnortParser.is_valid_rule(line):
                    rule_raw = line.strip()
                    rule_instance = SnortParser(rule_raw)
                    rules.append(rule_instance)
            return rules

    except OSError:
        return "file not found"


def add_rule_db(msg: str, sid: int, cve: int = None):
    sql = ("INSERT IGNORE INTO db.rules(msg, sid, cve) VALUES (%s, %s, %s) ")
    cur.execute(sql, (msg, sid, cve,))
    con.commit()


def get_rules_db():
    sql = ("SELECT * FROM db.rules ORDER BY sid DESC")
    cur.execute(sql)
    result = cur.fetchall()
    return result


def delete_rule_db(sid):
    sql = ("DELETE FROM db.rules WHERE sid = %s")
    cur.execute(sql, (sid,))
    con.commit()
    print(f"Remove rule sid: {sid}")


def delete_rules_db():
    sql = ("DELETE FROM db.rules")
    print("Dropping data from table")
    cur.execute(sql)
    con.commit()


def main():
    rules = read_rules(TEST_DATA)
    # if type(rules) == list and con.is_connected():
    #     [add_rule_db(rule.msg, rule.sid, rule.cve) for rule in rules]
    # get_rule_db()
    print(rules)


if __name__ == '__main__':
    main()


# todo add integration tests for db connection on gitlab + local
# todo add docstrings to snortparser class + instance methods

# GOAL - have a small project on gitlab which utilises unittests and integration tests for CI pipeline
