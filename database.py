import os
import mysql.connector

config = {
    "user": "user",
    "password": "password",
    "host": "localhost",
    "database": "db"
}

if os.getenv('MY_SQL_DATABASE'):
    config['database'] = 'testdb'
    config['host'] = '127.0.0.1'
    config['user'] = 'root'
    config['password'] = 'mysql'

con = mysql.connector.connect(**config)
cur = con.cursor()